﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ImgApp
{
    public class DatabaseHandler
    {
        public SQLiteAsyncConnection dbAsync;

        public SQLiteAsyncConnection GetConn(string db, string pwd)
        {
            var options = new SQLiteConnectionString(db, true,
                    key: pwd,
                    preKeyAction: db => db.Execute("PRAGMA cipher_default_use_hmac = OFF;"),
                    postKeyAction: db => db.Execute("PRAGMA kdf_iter = 128000;"));

            dbAsync = new SQLiteAsyncConnection(options);

            string dbFullName = Directory.GetCurrentDirectory() + "\\" + db;

            if (File.Exists(dbFullName))
            {
                try
                {
                    var pokers = dbAsync.Table<Picture>().Where(pp => pp.Sn.Equals(1));

                    var result = pokers.ToListAsync();
                    if (result.Result == null) ;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error info");
                    Application.Exit();
                }
            }

            return dbAsync;
        }
    }
}
