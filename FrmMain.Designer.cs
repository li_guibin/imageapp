﻿
namespace ImgApp
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBoxRead = new System.Windows.Forms.PictureBox();
            this.edtSn = new System.Windows.Forms.TextBox();
            this.edtPwd = new System.Windows.Forms.TextBox();
            this.edtDb = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnGen = new System.Windows.Forms.Button();
            this.btnConn = new System.Windows.Forms.Button();
            this.picBoxWrite = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxWrite)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxRead
            // 
            this.picBoxRead.Location = new System.Drawing.Point(225, 63);
            this.picBoxRead.Name = "picBoxRead";
            this.picBoxRead.Size = new System.Drawing.Size(72, 96);
            this.picBoxRead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxRead.TabIndex = 17;
            this.picBoxRead.TabStop = false;
            // 
            // edtSn
            // 
            this.edtSn.Location = new System.Drawing.Point(136, 133);
            this.edtSn.Name = "edtSn";
            this.edtSn.Size = new System.Drawing.Size(69, 26);
            this.edtSn.TabIndex = 16;
            this.edtSn.Text = "1";
            // 
            // edtPwd
            // 
            this.edtPwd.Location = new System.Drawing.Point(499, 63);
            this.edtPwd.Name = "edtPwd";
            this.edtPwd.PasswordChar = '*';
            this.edtPwd.Size = new System.Drawing.Size(131, 26);
            this.edtPwd.TabIndex = 15;
            this.edtPwd.Text = "abc321";
            // 
            // edtDb
            // 
            this.edtDb.Location = new System.Drawing.Point(353, 63);
            this.edtDb.Name = "edtDb";
            this.edtDb.Size = new System.Drawing.Size(131, 26);
            this.edtDb.TabIndex = 14;
            this.edtDb.Text = "ImgApp.db";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(201, 207);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(96, 29);
            this.btnLoad.TabIndex = 13;
            this.btnLoad.Text = "load image";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(33, 207);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(104, 29);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "save image";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnGen
            // 
            this.btnGen.Location = new System.Drawing.Point(499, 120);
            this.btnGen.Name = "btnGen";
            this.btnGen.Size = new System.Drawing.Size(131, 29);
            this.btnGen.TabIndex = 11;
            this.btnGen.Text = "gen";
            this.btnGen.UseVisualStyleBackColor = true;
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            // 
            // btnConn
            // 
            this.btnConn.Location = new System.Drawing.Point(353, 120);
            this.btnConn.Name = "btnConn";
            this.btnConn.Size = new System.Drawing.Size(131, 29);
            this.btnConn.TabIndex = 10;
            this.btnConn.Text = "conn";
            this.btnConn.UseVisualStyleBackColor = true;
            this.btnConn.Click += new System.EventHandler(this.btnConn_Click);
            // 
            // picBoxWrite
            // 
            this.picBoxWrite.Image = global::ImgApp.Properties.Resources.back;
            this.picBoxWrite.Location = new System.Drawing.Point(33, 63);
            this.picBoxWrite.Name = "picBoxWrite";
            this.picBoxWrite.Size = new System.Drawing.Size(71, 96);
            this.picBoxWrite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxWrite.TabIndex = 9;
            this.picBoxWrite.TabStop = false;
            this.picBoxWrite.Click += new System.EventHandler(this.picBoxWrite_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(353, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 18);
            this.label1.TabIndex = 18;
            this.label1.Text = "DB Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(499, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 18);
            this.label2.TabIndex = 19;
            this.label2.Text = "Password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "Import image:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 18);
            this.label4.TabIndex = 21;
            this.label4.Text = "Read image:";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 262);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.picBoxRead);
            this.Controls.Add(this.edtSn);
            this.Controls.Add(this.edtPwd);
            this.Controls.Add(this.edtDb);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnGen);
            this.Controls.Add(this.btnConn);
            this.Controls.Add(this.picBoxWrite);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "sava and load image - sqlite";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxWrite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxRead;
        private System.Windows.Forms.TextBox edtSn;
        private System.Windows.Forms.TextBox edtPwd;
        private System.Windows.Forms.TextBox edtDb;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnGen;
        private System.Windows.Forms.Button btnConn;
        private System.Windows.Forms.PictureBox picBoxWrite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

