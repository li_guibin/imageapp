﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SQLite;

namespace ImgApp
{
    public partial class FrmMain : Form
    {
        private DatabaseHandler _dbHandler;
        public FrmMain()
        {
            InitializeComponent();
            _dbHandler = new DatabaseHandler();
        }

        private async Task<MemoryStream> LoadImageAsync(int iSn)
        {
            byte[] myData = null;

            var photos = _dbHandler.dbAsync.Table<Picture>().Where(pic => pic.Sn.Equals(iSn));

            var result = await photos.ToListAsync();

            foreach (var factoryCar in result)
            {
                myData = factoryCar.Photo;
            }

            return new MemoryStream(myData);
        }

        private void SaveImage(int sn, Image image, ImageFormat format)
        {
            using MemoryStream ms = new MemoryStream();
            string sql = "insert into Picture(sn, photo) values(?, ?)";

            // Convert Image to byte[]
            image.Save(ms, format);
            byte[] imageBytes = ms.ToArray();

            try
            {
                _dbHandler.dbAsync.ExecuteScalarAsync<Picture>(sql, sn, imageBytes);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error info");
            }
        }

        private void btnConn_Click(object sender, EventArgs e)
        {
            string strDb = edtDb.Text;
            string strPwd = edtPwd.Text;

            if ((strDb != null) && (strPwd != null))
                _dbHandler.GetConn(strDb, strPwd);
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            _dbHandler.dbAsync.CreateTableAsync<Picture>();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int.TryParse(edtSn.Text, out int i);

            SaveImage(i, picBoxWrite.Image, ImageFormat.Png);
            edtSn.Text = (i + 1).ToString();
        }

        private async void btnLoad_Click(object sender, EventArgs e)
        {
            int.TryParse(edtSn.Text, out int i);
            picBoxRead.Image = Image.FromStream(await LoadImageAsync(i));
            edtSn.Text = (i - 1).ToString();
        }

        private void picBoxWrite_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Picture File (*.png)|*.png";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
                picBoxWrite.ImageLocation = FileName;
            }
        }
    }
}
