﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImgApp
{
    class Picture
    {
        [PrimaryKey]
        [Column("sn")]
        public int Sn { get; set; }

        [Column("photo")]
        public byte[] Photo { get; set; }
    }
}
