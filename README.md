# Image WinForms Application
  Use [sqlite-net](https://github.com/praeclarum/sqlite-net) to manipulate sqlite.

- Dotnet:   v3.1(core)+    
- Language：C Sharp    

## NuGet
> Package Manager Console

```bash
Install-Package sqlite-net-pcl
Install-Package sqlite-net-sqlcipher
```

## How to classify pictures?
Calculation principle: The digits of sn are the same!    
For example, Class A is 1xx; 
And so on to 9xx.
